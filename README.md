<a name="readme-top"></a>

<!-- PROJECT LOGO -->
<div align="center">
  <h1 align="center">Malik Omar's Portfolio</h1>
</div>

## About The README

This readme will include tools I used, linting rules, and setup instructions.

### Built With

This portfolio was built with

- [![React][React.js]][React-url]
- [![TailwindCSS][TailwindCSS-logo]][TailwindCSS-url]

Deployed on

- [![Netlify][netlify-logo]][netlify-url]

## Linting rules (prettier)

```json
{
  "files.autoSave": "onFocusChange",
  "editor.defaultFormatter": "esbenp.prettier-vscode",
  "explorer.confirmDragAndDrop": false,
  "editor.fontSize": 18,
  "editor.fontWeight": "100",
  "files.trimTrailingWhitespace": true,
  "prettier.jsxSingleQuote": true,
  "prettier.singleQuote": true,
  "prettier.withNodeModules": true,
  "javascript.updateImportsOnFileMove.enabled": "always",
  "editor.formatOnPaste": true,
  "editor.formatOnSave": true
}
```

### Installation

If you want to run my portfolio on your local machine, you can follow these steps

1. Clone the repo
   ```sh
   git clone https://gitlab.com/MalikOmar/myportfolio.git
   ```
2. Navigate to the correct directory
   ```sh
   cd client
   ```
3. Install NPM packages
   ```sh
   npm install
   ```
4. Start the server (PORT 3000)
   ```sh
   npm start
   ```

Open [http://localhost:3000](http://localhost:3000) to view it in your browser.

To stop the server

`CTRL + C` at the command line

## Acknowledgments

- [Img Shields](https://shields.io)
- [React Icons](https://react-icons.github.io/react-icons)

<p align="right">(<a href="#readme-top">back to top</a>)</p>

## Contact

- [![LinkedIn][linkedin-shield]][linkedin-url]

<!-- MARKDOWN LINKS & IMAGES -->

[React.js]: https://img.shields.io/badge/React-20232A?style=for-the-badge&logo=react&logoColor=61DAFB
[React-url]: https://react.dev/
[TailwindCSS-logo]: https://img.shields.io/badge/TailwindCSS-20232A?style=for-the-badge&logo=tailwindcss&logoColor=%2306B6D4
[TailwindCSS-url]: https://tailwindcss.com
[netlify-logo]: https://img.shields.io/badge/netlify-20232A?style=for-the-badge&logo=netlify&logoColor=%2300C7B7
[netlify-url]: https://www.netlify.com
[linkedin-shield]: https://img.shields.io/badge/linkedin-white?style=for-the-badge&logo=linkedin&logoColor=%230A66C2
[linkedin-url]: https://linkedin.com/in/malikomarswe
